<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/images/2.png" type="shop/x-icon">
    <title>STAFF</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Styles -->
    <link rel="stylesheet" href="https://laravelecommerceexample.ca/css/app.css">
    <link rel="stylesheet" href="https://laravelecommerceexample.ca/css/responsive.css">
    <link rel="stylesheet" href="{{ asset('css/about.css') }}">


</head>
<body>
<div id="app">
    <header class="with-background">
        <div class="top-nav container">
            <div class="top-nav-left">
                <div class="logo"><a href="https://www.staff-clothes.com/">STAFF</a></div>
                <ul>
                    <li>
                        <a href={{'/'}}>
                            Головна
                        </a>
                    </li>
                    <li>
                        <a href={{'/#shop'}}>
                            Магазин
                        </a>
                    </li>
                    <li>
                        <a href={{'/about'}}>
                            Про нас
                        </a>
                    </li>
                </ul>

            </div>
            <div class="top-nav-right">
                <ul>
                    <li><a href="{{ asset('register') }}">Реєстрація</a></li>
                    <li><a href="{{ asset('login') }}">Логін</a></li>
                    <li><a href={{'/basket'}}><i class="fa fa-shopping-cart"> Корзина</i></a></li>
                    @auth
                    <li><a href="{{ asset('logout') }}">Вийти</a></li>
                    @endauth
                </ul>
            </div>
        </div> <!-- end top-nav -->
        <div class="hero container">
            <div class="hero-copy">
                <p><h2>Ставай краще з кожним днем - це слоган, який є синонімом бренду STAFF. <br>
                    Ми любимо свою справу!</h2></p>
            </div> <!-- end hero-copy -->

            <div class="hero-image">
                <style>
                    img {
                        border-radius: 8px;
                    }

                </style>
                <img src="/images/staff_1.jpg" alt="hero image" height="400" width="1332" border-radius: 50%>
            </div> <!-- end hero-image -->
        </div> <!-- end hero -->
    </header>

    <div class="featured-section">

        <div class="container">
            <h1 class="text-center">STAFF</h1>

            <p class="section-description"><center><strong><font color="#696969">Обираючи Staff,
                    ви перш за все отримуєте якість за доступну ціну.
                    Ми цінуємо кожного з наших покупців, пишаємося великою кількістю позитивних відгуків.
                    Технології розробки та випуску продукції безперервно вдосконалюються -
                    наші клієнти заслуговують лише найкращого.
                    Здійснюємо суворий контроль виробництва і підбору тканин для нового одягу.</font></strong></center>
            </p>
            <br>
            <br>
            <div class="products text-center" id="shop">
                @foreach($products as $product)
                    <div class="product">
                        <a href={{'show/' . $product->id_goods}}><img src={{$product->image_path}} alt="product"></a>
                        <a href={{'show/' . $product->id_goods}}><div class="product-name">{{$product->name}}</div></a>
                        <div class="product-price"><font color="black">{{$product->price}} ₴</font></div>
                    </div>
                @endforeach

            </div> <!-- end products -->

            <div class="text-center button-container">
                <a href={{'more_products'}}><i class="button">Більше товарів</i></a>
            </div>

        </div> <!-- end container -->

    </div> <!-- end featured-section -->

    <footer>
        <div class="footer-content container">
            <div class="footer__copyright">© <?php $date = date('Y'); echo $date;?> Staff</div>
            <ul>
                <li>Follow Me:</li>
                <li><a href=""><i class="fa Follow Me:"></i></a></li>
                <li><a href="https://www.youtube.com/"><i class="fa fa-youtube"></i></a></li>
                <li><a href="https://gitlab.com/"><i class="fa fa-gitlab"></i></a></li>
                <li><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://vk.com/id198459658"><i class="fa fa-vk"></i></a></li>
            </ul>

        </div> <!-- end footer-content -->
    </footer>

</div> <!-- end #app -->
<script src="js/app.js"></script>
</body>
</html>