@extends('master')

@section('content')
    <br>
    <div class="about-staff__bg">
        <style>
            img {
                border-radius: 8px;
            }

        </style>
        <img src="/images/10.jpg" width="1084" height="400" border-radius: 50%>
<div class="about-staff__content-center">
    <div class="about-staff__about">
        <br>
        <div class="about-staff__about-column"><p align="middle"><strong>Історія компанії Staff почалася в 2013 році.
                Засновники - енергійні та креативні хлопці,
                брати з невеликого міста в західній частині України, -
                вирішили направити свою діяльність на розвиток української streetwear культури.
                Восени того ж року першим релізом була випущена парку під назвою Staff.
                Вона стала першою ластівкою з широкого асортименту розроблених і
                втілених компанією моделей.
                Українці підтримали молодіжний бренд і вітали виходи новинок.<br><br>
                У 2015 році колекції бренду Staff включали в себе вже як верхній одяг, так і взуття.
                Кредо компанії - постійний рух вперед.
                    З кожним релізом ми ставимо перед собою нові завдання і працюємо на максимальний результат.</strong></p>
        </div>
        <div class="about-staff__about-column"><p align="middle"><strong>Вибираючи Staff, ви перш за все отримуєте якість за доступну ціну.
            Ми цінуємо кожного з наших покупців, пишаємося великою кількістю позитивних відгуків.
            Технології розробки та випуску продукції безперервно вдосконалюються - наші клієнти гідні найкращого.
            Здійснюємо суворий контроль виробництва і підбору тканин для нового одягу.
            <br><br>
            Над розвитком українського бренду одягу потужно працює команда талановитих і компетентних молодих людей, ентузіастів своєї справи.
            <br><br>Сьогодні бренд Staff представлений у багатьох містах України.
            <br><br>У березні 2018 вперше ми випустили жіночу колекцію, яка стала черговим підтвердженням того що постійний розвиток -
                    головна риса компанії.</strong></p><br>
            </div>
        </div>
    </div>
    <div class="about-staff__gray-bg">
        <div class="about-staff__content-center">
            <div class="about-staff__be-better">Ставай краще<br>з кожним днем
                </div>
            <div class="about-staff__slogan-desc">– це слоган, який є синонімом бренду STAFF.<br>Ми любимо свою справу</div>
            <div class="about-staff__best-regards">З повагою команда<a href="https://www.staff-clothes.com/"><u> #Staffclothes</u></a></div>
        </div>
    </div>
</div>
@endsection
