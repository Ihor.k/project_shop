@extends('master')

@section('content')

<div class="products-section container">
    <div class="sidebar">
        <h3><strong>Категорії</strong></h3>
        <ul>
            @foreach($categories = $collect['categories'] as $category)
            <li><a href={{'/category/' . $category->id_category}}>{{$category->name}} ({{$category->quantity}})</a>
            </li>
            @endforeach
        </ul>

    </div> <!-- end sidebar -->
    <div>
        <div class="products-header">
            <h1 class="stylish-heading">Всі категорії</h1>

        </div>

        <div class="products text-center">
            @foreach($products = $collect['products'] as $product)
                <div class="product">
                    <style>
                        img {
                            border-radius: 8px;
                        }

                    </style>
                    <a href="{{'/show/' . $product->id_goods}}"><img src={{$product->image_path}} alt="product" border-radius: 50%></a>
                    <div class="product-name"><a href="{{'/show/' . $product->id_goods}}">{{$product->name}}</a></div>
                    <div class="product-price"><font color="black">{{$product->price}} ₴</font></div>

                </div>
            @endforeach

        </div> <!-- end products -->
        {{ $products->links() }}

    </div>
</div>

@endsection
