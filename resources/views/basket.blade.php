@extends('master')

@section('content')
<head>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/starter-template.css') }}">
</head>
    <div class="starter-template">
        <p class="alert alert-warning"></p>
        <h1>Корзина</h1>
        <p>Оформлення товару</p>
        <div class="panel">
            <table class="table table-striped">
                @foreach($products ?? [] as $product)
                <thead>
                <tr>
                    <th>Назва</th>
                    <th>Кількість</th>
                    <th>Ціна</th>
                    <th>До сплати</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <a href="{{$product->id_goods}}">
                                <img height="56px" src="{{$product->image_path}}" alt="product">
                            </a>
                        </td>
                        <td><span class="badge">{{ \App\Basket::find(Auth::user()->id)->count }}</span>
                            <div class="btn-group form-inline">
                                <form action="{{ route('basket-remove', $product) }}" method="POST">
                                    <button type="submit" class="btn btn-danger"><span
                                                class="glyphicon glyphicon-minus" aria-hidden="true"></span></button>
                                    @csrf
                                </form>
                                <form action="{{ route('basket-add', $product) }}" method="POST">
                                    <button type="submit" class="btn btn-success"><span
                                                class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                                    @csrf
                                </form>
                            </div>
                        </td>
                        <td>{{ $product->price }} грн.</td>
                        <td>{{ $product->price * \App\Basket::find(Auth::user()->id)->count }} грн.</td>
                    </tr>
                    @endforeach
                    <tr>
                    <td>Загальна вартість:</td>
                    <td>{{ isset($product) ? $product->price * \App\Basket::find(Auth::user()->id)->count : 0 }} грн.</td>
                </tr>
                </tbody>
            </table>
            <br>
            <div class="btn-group pull-right" role="group">
                <a type="button" class="btn btn-success" href="{{ route('basket-order') }}">Оформити товар</a>
            </div>
        </div>
    </div>

{{--<div class="might-like-section">--}}
{{--    <div class="container">--}}
{{--        <h2>Вам може сподобатися...</h2>--}}
{{--        <div class="might-like-grid">--}}
{{--            @foreach($products as $product)--}}
{{--                <div class="might-like-product">--}}
{{--                    <a href={{'/show/' . $product->id_goods}}><img src={{ $product->image_path }} alt="product"></a>--}}
{{--                    <a href={{'/show/' . $product->id_goods}}><div class="might-like-product-name">{{ $product->name }}</div></a>--}}
{{--                    <div class="might-like-product-price"><font color="black">{{ $product->price }} ₴</font></div>--}}
{{--                </div>--}}
{{--            @endforeach--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

@endsection