<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="MlIJG2nZSxlW7wuvq3SyuGemz69IdSiNw2ePg7M2">

    <title>STAFF</title>

    <link rel="shortcut icon" href="/images/2.png" type="shop/x-icon">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Styles -->
    <link rel="stylesheet" href="https://laravelecommerceexample.ca/css/app.css">
    <link rel="stylesheet" href="https://laravelecommerceexample.ca/css/responsive.css">

    <link rel="stylesheet" href="https://laravelecommerceexample.ca/css/algolia.css">
    <link rel="stylesheet" href="{{ asset('css/about.css') }}">

</head>

<body class="">
<header>
    <div class="top-nav container">
        <div class="top-nav-left">
            <div class="logo"><a href="https://www.staff-clothes.com/">STAFF</a></div>
            <ul>
                <li>
                    <a href={{'/'}}>
                        Головна
                    </a>
                </li>
                <li>
                    <a href={{'/#shop'}}>
                        Магазин
                    </a>
                </li>
                <li>
                    <a href={{'/about'}}>
                        Про нас
                    </a>
                </li>
            </ul>

        </div>
        <div class="top-nav-right">
            <ul>
                    <li><a href="{{ asset('register') }}">Реєстрація</a></li>
                <li><a href="{{ asset('login') }}">Логін</a></li>
                <li><a href={{'/basket'}}><i class="fa fa-shopping-cart"> Корзина</i></a></li>
                @auth
                <li><a href="{{ asset('logout') }}">Вийти</a></li>
                @endauth
            </ul>
        </div>
    </div> <!-- end top-nav -->
</header>
<div>
    @yield('content')
</div>

<footer>
    <div class="footer-content container">
        <div class="footer__copyright">© <?php $date = date('Y'); echo $date;?> Staff</div>
        <ul>
            <li>Follow Me:</li>
            <li><a href=""><i class="fa Follow Me:"></i></a></li>
            <li><a href="https://www.youtube.com/"><i class="fa fa-youtube"></i></a></li>
            <li><a href="https://gitlab.com/"><i class="fa fa-gitlab"></i></a></li>
            <li><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://vk.com/id198459658"><i class="fa fa-vk"></i></a></li>
        </ul>

    </div> <!-- end footer-content -->
</footer>
