@extends('master')

@section('content')
    <head>
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/starter-template.css') }}">
    </head>
    <div class="starter-template">
        <h1><b>Підтвердіть замовлення!</b></h1>
        <div class="container">
            <div class="row justify-content-center">
{{--                <p>Загальна сума замовлення: {{ isset($product) ? $product->price * \App\Order::find(Auth::user()->id)->count : 0 }} грн.<b></b></p>--}}
                <form action="{{ route('basket-confirm') }}" method="POST">
                    <div>
                        <div class="container">
                            <div class="form-group">
                                <label for="name" class="control-label col-lg-offset-3 col-lg-2">Прізвище, ім'я: </label>
                                <div class="col-lg-4">
                                    <input type="text" name="name" id="name" value="" class="form-control">
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label for="phone" class="control-label col-lg-offset-3 col-lg-2">Номер телефона: </label>
                                <div class="col-lg-4">
                                    <input type="text" name="phone" id="phone" value="" class="form-control">
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label for="phone" class="control-label col-lg-offset-3 col-lg-2">Місто: </label>
                                <div class="col-lg-4">
                                    <input type="text" name="town" id="town" value="" class="form-control">
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label for="phone" class="control-label col-lg-offset-3 col-lg-2">Номер віділення пошти: </label>
                                <div class="col-lg-4">
                                    <input type="text" name="department_post" id="department_post" value="" class="form-control">
                                </div>
                            </div>
                        </div>
                        <br>
                        <input type="hidden" name="_token" value="MBOKX5wVH7oZaOtCARv1iiD3i5HlDXeJzyLpD6l7">
                        <br>
                        @csrf
                        <input type="submit" class="btn btn-success" value="Підтвердити замовлення">
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection