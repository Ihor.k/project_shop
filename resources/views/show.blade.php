@extends('master')

@section('content')

<div class="product-section container">
        <div class="product-section-image">
            <img src={{ $product->image_path }} alt="product" class="active" id="currentImage">
        </div>
    <div class="product-section-information">
        <h1 class="product-section-title">{{ $product->name }}</h1>
        @if ($product->is_avail)
            <div class="badge badge-success">In Stock</div>
        @else
            <div class="badge badge-danger">Out of Stock</div>
        @endif
        <div class="product-section-price">{{$product->price}} ₴</div>

        <p>
           {{ $product->description }}
        </p>


        <form action="{{ route('basket-add', $product) }}" method="post">
            @csrf
            @if($product->is_avail)
                <button type="submit" class="button button-plain" role="button">В корзину</button>
                @elseif($product->is_avail=null)
                <button type="submit" class="btn-block" role="button">В корзину</button>
            @endif
        </form>
    </div>
</div> <!-- end product-section -->

@endsection
