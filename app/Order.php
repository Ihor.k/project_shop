<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'status',
        'name',
        'phone',
        'town',
        'department_post',
    ];

    public function products()
    {

    }

    public function getFullPrice($count, $price){
        return $count * $price;
    }

//    public function products()
//    {
//        return $this->belongsToMany(Product::class)->withPivot('count')->withTimestamps();
//    }
//
}
