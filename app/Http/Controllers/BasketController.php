<?php

namespace App\Http\Controllers;

use App\Basket;
use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BasketController extends Controller
{
    public function basket()
    {
        $basketId = Auth::user()->id;
        if (!is_null($basketId)){
            if ( $order = Basket::find($basketId)){
            $products = [Product::find($order->product_id)];
            return view('basket',compact('products'));
            }
        }
        return view('basket',[]);
    }

    public function basketOrder(){
//        $orderId = session('orderId');
//        if(is_null($orderId)){
//            return redirect()->route('index');
//        }
//        $order = Order::find($orderId);
        return view('order');
    }

    public function basketConfirm(Request $request)
    {
//        $orderId = session('orderId');
//        if(is_null($orderId)){
//            return redirect('/');
//        }
        $order = Order::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'town' => $request->town,
            'department_post' => $request->department_post,
            'status' => 1,
        ]);
//        $order->name = $request->name;
//        $order->phone = $request->phone;
//        $order->town = $request->town;
//        $order->department_post = $request->department_post;
//        $order->status = 1;
//        $order->save();
//
       return redirect('/');
    }

    public function basketAdd($productId)
    {
        $basketId = Auth::user()->id;
        if(!$basket = Basket::find($basketId)){
            $basket = Basket::create([
                'basket_id' => $basketId,
                'product_id' => $productId,
                'count' => 1,
            ]);
        } else {
            $basket->count++;
            $basket->save();
        }
//        session(['orderId' => $basket->id]);

        $product = Product::find($productId);
        session()->flash('success', 'Товар додано:' . $product->name);

        return redirect('basket');
    }

    public function basketRemove($productId)
    {
        $orderId = Auth::user()->id;
        if(is_null($orderId)){
            return redirect('basket');
        }
        $order = Basket::find($orderId);
        if ($order->count == 1){
            $order->delete();
        } else {
            $order->count--;
            $order->save();
        }
//        if ($order->products->contains($productId)){
//            $pivotRow = $order->products()->where('product_id', $productId)->first()->pivot;
//            if ($pivotRow->count < 2){
//                $order->products()->detach($productId);
//            } else {
//                $pivotRow->count--;
//                $pivotRow->update();
//            }
//        }
//        $product = Product::find($productId);
        session()->flash('warning', 'Товар видалено');
        return redirect('basket');
    }
}
