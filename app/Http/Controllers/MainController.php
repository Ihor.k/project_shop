<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Product, App\Category;
use Illuminate\Http\Request;
use App;
class MainController extends Controller
{
    public function index(){
        $products = DB::table('products')->limit(10)->get();
        return view('index',compact('products'));
    }

    public function show($id){

        return view('show', [
            'product' => Product::all()->find($id),
        ]);
    }

    public function category($id){

        return view('category', [
            'collect' => [
                'categories' => Category::all(),
                'category' => Category::find($id),
                'products' => DB::table('categories')
                    ->join('product_category', 'categories.id_category', '=', 'product_category.id_category')
                    ->join('products', 'products.id_goods', '=', 'product_category.id_goods')
                    ->where('categories.id_category', $id)
                    ->select('products.*')
                    ->paginate(9),
            ],
        ]);
    }

    public function more_products(){
        $categories = DB::table('categories')->get();
        return view('more_products',[
            'collect' => [
                'categories' => App\Category::all(),
                'products' => App\Product::paginate(6),
            ]
        ]);
    }

    public function about(){
        return view('about');
    }

}
