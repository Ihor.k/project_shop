<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class  Product extends Model
{
    protected $table = 'products';
    protected $primaryKey = 'id_goods';

    public function getPriceForCount(){
        if(!is_null($this->pivot)){
            return $this->pivot->count * $this->price;
        }
        return $this->price;
    }
}
