<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{
    protected $fillable = [
        'basket_id',
        'product_id',
        'count',
    ];

    protected $primaryKey = 'basket_id';

    public function products()
    {
//        return $this->belongsToMany(Product::class, 'products', 'id_goods', 'product_id')
//            ->withPivot('count')->withTimestamps();
    }

    public function getFullPrice($count, $price){
        return $count * $price;
    }
}
