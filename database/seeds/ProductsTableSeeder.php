<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Product::class, 50)->create();

//        DB::table('products')->insert([
//            'name' => 'Google Pixel 2 XL',
//            'description' => 'New condition • No returns, but backed by eBay Money back guarantee',
//            'image_path' => 'https://i.ebayimg.com/00/s/MTYwMFg4MzA=/z/G2YAAOSwUJlZ4yQd/$_35.JPG?set_id=89040003C1',
//            'price' => 675.00,
//            'quantity' => 55,
//            'is_avail' => true,
//        ]);
    }
}
