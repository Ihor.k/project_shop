<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->text(255),
        'description' => $faker->text,
        'image_path' => 'no-image.png',
        'price' => $faker->numberBetween(1, 1000),
        'quantity' => $faker->numberBetween(0, 20),
        'is_avail' => $faker->boolean,
    ];
});
