<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes([
    'reset' => false,
    'confirm' => false,
    'verify' => false,
]);

Route::group(['middleware' => 'auth'], function () {
    Route::get('/logout', 'Auth\LoginController@logout');

    Route::get('/home', 'HomeController@index');

    Route::get('/', 'MainController@index');

    Route::get('more_products', 'MainController@more_products');

    Route::get('/basket', 'BasketController@basket');

    Route::post('/basket/add/{id}', 'BasketController@basketAdd')->name('basket-add');

    Route::post('/basket/remove{id}', 'BasketController@basketRemove')->name('basket-remove');

    Route::get('/basket/order', 'BasketController@basketOrder')->name('basket-order');

    Route::post('/basket/order', 'BasketController@basketConfirm')->name('basket-confirm');

    Route::get('category/{id}', 'MainController@category');

    Route::get('show/{id}', 'MainController@show');

    Route::get('/about', 'MainController@about');

});

